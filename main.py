"""
1. After successful build rename the *.hex file to *.img file
2. Create sha of that file and save in *.hsh format
3. Try to save that file in drive name: "FIRMWARE".
4. Wait for 2 minutes until that drive is found.
"""
import os
import hashlib
from shutil import copyfile
from sys import exit

location = "E:\\1. Projects\\2. ST\\3. GreyLock\\greylock_app\\Release\\"  # Release
# location = "E:\\1. Projects\\0. Personal\\FAHIM Tools\\2. GreyLock hash creator\\"  # Debug

os.chdir(location)  # Enter destination

fileName = "Greylock_Application.hex"

# Split file name and extension
firmwareFileName = fileName.split(".")
fullImageFileName = f"{firmwareFileName[0]}.img"
fullHashFileName = f"{firmwareFileName[0]}.hsh"

# Copy *.hex file and paste as *.img file
try:
    copyfile(fileName, fullImageFileName)
except IOError as e:
    print(f"Unable to copy file. {e}")
    exit(1)
except Exception:
    print("Unexpected error:", sys.exc_info())
    exit(1)

# Calculate SHA1 of image file
firmwareFileObj = open(fullImageFileName, "rb")
buf = firmwareFileObj.read()
hash_object = hashlib.sha1(buf)
sha1OfFirmwareFile = hash_object.hexdigest().upper()
print("Location of the FW\t:\t" + location + fileName)
print("SHA1 of the FW\t\t:\t" + sha1OfFirmwareFile)

with open(fullHashFileName, "w") as hashFileObj:
    hashFileObj.write(sha1OfFirmwareFile)  # Write file
print(
    "\nSuccessfully created following files: \n"
    + fullHashFileName
    + "\n"
    + fullImageFileName
)
